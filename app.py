#!flask/bin/python
from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/postjson', methods=['POST'])
def post():
    print(request.is_json)
    content = request.get_json()
    #  print(content)
    print(content['id_001'])
    print(content['jorgescalona'])
    #  return 'Hello, World!'
    return 'JSON posted'

#   if __name__ == '__main__':
app.run(host='0.0.0.0', port=5000)
