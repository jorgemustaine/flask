[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

# Flask

Repository for educational purpose of technologies like Flask, Jinja an API REST

Flask is a microframework python based. It has two major components: **Werkzeug and
jinja2**.

 for install:  `pip install flask`

## REST

Representational State Transfer (REST) is a software architecture style for web
services that provides a standard for data communication between different kind
of systems. **WebService:** in a simple definition is a service offered by one electronic
device to another via WWW (World Wide Web).

![verbs REST](static/images/verbs_api_rest_00.png "verb REST")

REST is a useful for build microservices or consume those microservices.

## NoSQL Databases

Have a dynamic schema for unstructured data and store data in different ways ranging
from column-based (Apache Cassandra), document based (MongoDB), and graph-based
(Neo4) or as key-value(Redis). This provide flexibility to store data without a
predefined structure and versatility to add fields to the data structure on the go.

@jorgemustaine 2019 / 2020

### reference:

* [flask essentials](http://flask.pocoo.org/)
* [flask tutorial](http://flask.pocoo.org/docs/1.0/tutorial/)
* [For mock API](https://designer.mocky.io/)
* [Install MySQL on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/como-instalar-mysql-en-ubuntu-18-04-es)
* [code for boot repo in github](https://github.com/Apress/building-rest-apis-with-flask)
