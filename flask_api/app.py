import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

mysql_username = os.environ.get('mysql_username')
mysql_password = os.environ.get('mysql_password')
mysql_port = os.environ.get('mysql_port')
mysql_db = os.environ.get('mysql_db')
mysql_host = os.environ.get('mysql_host')

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] =\
    'mysql+pymysql://' + mysql_username + ':' + mysql_password + \
    '@' + mysql_host + ':' + mysql_port + '/' + mysql_db
db = SQLAlchemy(app)


class Authors(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))
    specialisation = db.Column(db.String(50))

    def __init__(self, name, specialisation):
        self.name = name
        self.specialisation = specialisation

    def __repr__(self):
        return '<Author %d>' % self.id


db.create_all()

if __name__ == "__main__":
    app.run(debug=True)
