from flask import Flask, render_template
import connexion

#  Create application instance
app = Flask(__name__, template_folder="templates")

#  Create a URL route in our application for "/"
@app.route('/')
def home():
    """
    This function just response to the browser url
    localhost:5000/
    """
    return render_template('home.html')

#  if we're running in standalone mode, run the application
if __name__ == '__main__':
    app.run(debug=True)
